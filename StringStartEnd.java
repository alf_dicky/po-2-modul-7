package Modul7;

public class StringStartEnd
{
    public static void main(String[] args){
        String[] strings = {"started", "starting", "ended", "ending"};
        for(String string : strings){
            if(string.startsWith("st"))
                System.out.println(string+" starts with \"st\"");
        }
        System.out.println("Coba startAt dengan menentukan posisi awal:");
        for(String string : strings){
            if(string.startsWith("art",2))
                System.out.println(string+" starts with \"art\" at posisiton 2");
        }
        System.out.println("Coba endsAt:");
        for(String string : strings){
            if(string.endsWith("ed"))
                System.out.println(string+" end with \"ed\"");
        }

    }
}
