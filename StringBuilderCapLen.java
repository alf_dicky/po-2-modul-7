package Modul7;

public class StringBuilderCapLen
{
    public static void main(String[] args){
        StringBuilder buffer = new StringBuilder("Hello, how are you?");
        System.out.println("buffer = "+buffer.toString()+"\nlength = "+buffer.length()+"\ncapacity = "+buffer.capacity());
        buffer.ensureCapacity(75);
        System.out.println("new capacity = "+buffer.capacity()+"\n\n");
        buffer.setLength(10);
        System.out.println("New Length = "+buffer.length()+"\nbuffer = "+buffer.toString());
    }
}
