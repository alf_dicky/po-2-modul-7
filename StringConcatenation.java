package Modul7;

public class StringConcatenation
{
    public static void main(String[] args){
        String s1 = "Happy";
        String s2 = "Birthday";
        System.out.println("s1 = "+s1+"\n"+"s2 = "+s2);
        System.out.println("Hasil dari method s1.concat(s2) adalah "+s1.concat(s2));
        System.out.println("s1 setelah concatenation = "+s1);
    }
}
