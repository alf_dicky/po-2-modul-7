package Modul7;

import javax.swing.JOptionPane;
public class TugasNo4
{
    public static void main(String[] args){
        int hasil = 0;
        int soal = 0;
        String soal1 = new String("Siapakah ketua jursan informatika?");
        String soal2 = new String("Dimana kampus informatika?");
        String soal3 = new String("Fakultas apa?");
        String soal4 = new String("Siapa yang mempunyai NIM 3411181055?");
        String soal5 = new String("Berapa NIM informatika angkatan 2018?");
        String jawaban1 = JOptionPane.showInputDialog(soal1);
        String jawaban2 = JOptionPane.showInputDialog(soal2);
        String jawaban3 = JOptionPane.showInputDialog(soal3);
        String jawaban4 = JOptionPane.showInputDialog(soal4);
        String jawaban5 = JOptionPane.showInputDialog(soal5);
        //periksa jawaban
        if(jawaban1.equals("Wina Witanti")){
            hasil = hasil+ 20;
            soal++;
        }
        if(jawaban2.equalsIgnoreCase("Cimahi")){
            hasil = hasil + 20;
            soal++;
        }
        if(jawaban3.equalsIgnoreCase("Sains dan Informatika")){
            hasil = hasil+ 20;
            soal++;
        }
        if(jawaban4.equalsIgnoreCase("Dicky Fajar Sani")){
            hasil = hasil+ 20;
            soal++;
        }
        if(jawaban5.equalsIgnoreCase("3411181")){
            hasil = hasil+ 20;
            soal++;
        }
        JOptionPane.showMessageDialog(null, "Anda telah berhasil menjawab "+soal+" soal \n Score akhir anda: "+hasil, "Score Akhir", JOptionPane.PLAIN_MESSAGE);
    }
}
