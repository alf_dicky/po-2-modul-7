package Modul7;

public class StringMiscellaneous2
{
    public static void main(String[] args){
        String s1 = "hello";
        String s2 = "GOODBYE";
        String s3 = "   spaces   ";
        System.out.println("s1 = "+s1+"\ns2 = "+s2+"\ns3 = "+s3);
        //coba method replace
        System.out.println("ganti l dengan L pada s1 = "+s1.replace('l', 'L'));
        //coba method toUpperCase dan toLowerCase
        System.out.println("s1.toUppercase = "+s1.toUpperCase());
        System.out.println("s2.toLowerCase = "+s2.toLowerCase());
        //coba methd trim
        System.out.println("s3 setelah ditrim = "+s3.trim());
        //coba method toCharArray
        char[] charArray = s1.toCharArray();
        System.out.println("s1 sebagai array of character: ");
        for(char character : charArray)
            System.out.print(character);
    }
}

