package Modul7;

public class StringIndexMethods
{
    public static void main(String[] args){
        String letters = "abcdefghijklmabcdefghijklm";
        //
        System.out.println("'c' berada pada index ke "+letters.indexOf('c'));
        System.out.println("'a' berada pada index ke "+letters.indexOf('a',1));
        System.out.println("'$' berada pada index ke "+letters.indexOf('$'));
        System.out.println();
        //Coba lastIndexOf
        System.out.println(" 'c' terakhir berada pada index ke "+letters.lastIndexOf('c'));
        System.out.println(" 'a' terakhir berada pada index ke "+letters.lastIndexOf('a',25));
        System.out.println(" '$' terakhir berada pada index ke "+letters.lastIndexOf('$'));
        System.out.println();
        //Coba indexOf to locate a substring
        System.out.println("\"def\" berada pada index ke "+letters.indexOf("def"));
        System.out.println("\"def\" berada pada index ke "+letters.indexOf("def",7));
        System.out.println("\"hello\" berada pada index ke "+letters.indexOf("hello"));
        System.out.println();
        System.out.println(" \"def\" terakhir berada pada index ke "+letters.lastIndexOf("def"));
        System.out.println(" \"def\" terakhir berada pada index ke "+letters.lastIndexOf("def",25));
        System.out.println(" \"hello\" terakhirberada pada index ke "+letters.lastIndexOf("hello"));
    }
}
