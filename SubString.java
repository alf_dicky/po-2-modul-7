package Modul7;

public class SubString
{
    public static void main(String[] args){
        String letters = "abcdefghijklmabcdefghijklm";
        System.out.println("Substring dari index ke 20 sampai akhir adalah "+letters.substring(20));
        System.out.println("Substring dari index ke 3 ke atas tapi tidak termasuk indeks ke 6 adalah "+letters.substring(3,6));
    }
}
